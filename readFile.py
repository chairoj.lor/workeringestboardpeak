import configparser
import pandas as pd
import os, glob, json

class readFile():
    def __init__(self):
        self.currentPath = ''
        self.movePath = ''
        self.directory = ''

    def getConfig(self):
        config = configparser.ConfigParser()
        config.read('config.conf')
        return config

    def setPath(self):
        config = self.getConfig()
        self.currentPath = config['CONDUCTOR']['currentPath']
        self.movePath = config['CONDUCTOR']['movePath']
        self.directory = config['CONDUCTOR']['directoryName']

    def readCsv(self):
        dataframe = pd.read_csv(self.directory)
        return dataframe

    def readData(self):
        dataframe = self.readCsv()
        directorys = os.listdir(self.currentPath)
        for directory in directorys:
            searchName = dataframe['name'].loc[dataframe['name'] == directory]

            if searchName.empty is True:
                dataframe = dataframe.append(
                    {'name': directory},
                    ignore_index = True
                )
            dataframe.to_csv(self.directory ,index=False)
    
    def deleteData(self, dataframe, asset_id):
        dataframe = dataframe.drop(asset_id, axis=1)
        dataframe.to_csv(self.directory ,index=False)
