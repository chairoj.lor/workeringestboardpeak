import json
from os import error
import requests
import configparser

class apiRequest():

    def __init__(self):
        self.url = ''
        self.headers = {}
        self.response = {}
        self.body = {}

    def setBody(self, body):
        self.body = body

    def setHeaders(self, headers):
        self.headers = headers
    
    def setUrl(self, url):
        self.url = url
    
    def setResponse(self, response):
        self.response = response
    
    def getUrlConfig(self):
        config = configparser.ConfigParser()
        config.read('config.conf')
        return config['CONDUCTOR']['url']

    def apiRequest(self, method):
        response = requests.request(method, self.url, headers=self.headers, data=self.body)
        return response.json()
    
    def apiRequestText(self, method):
        response = requests.request(method, self.url, headers=self.headers, data=self.body)
        return response.text

class conductorRequest(apiRequest):

    # def stringToJson(self):
    #     # self.response = json.dumps(self.response, indent=4, sort_keys=True)
    #     self.response = dict(self.response)
    #     return self.response

    def getTask(self, path):
        self.setUrl(self.getUrlConfig() + path)
        self.setResponse(self.apiRequest("get"))

    def updateTask(self):
        self.setUrl(self.getUrlConfig())
        asset_id = ''

        asset_id = self.response['inputData']['asset_id'].split('.')[0]

        body = {
            "workflowInstanceId": self.response['workflowInstanceId'],
            "taskId": self.response['taskId'],
            "reasonForIncompletion": "",
            "callbackAfterSeconds": 0,
            "workerId": "remoteCopy",
            "status": "COMPLETED",
            "outputData": {
                "ingest_status": "COMPLETED",
                "assetd": asset_id
            }
        }
        self.setBody(json.dumps(body))
        response = self.apiRequestText("post")
        return asset_id

    def updateTaskClearCache(self):
        self.setUrl(self.getUrlConfig())
        asset_id = ''

        asset_id = self.response['inputData']['fileName']

        body = {
            "workflowInstanceId": self.response['workflowInstanceId'],
            "taskId": self.response['taskId'],
            "reasonForIncompletion": "",
            "callbackAfterSeconds": 0,
            "workerId": "ClearCacheAll",
            "status": "COMPLETED",
            "outputData": {
                "ingest_status": "COMPLETED",
                "assetd": asset_id
            }
        }
        self.setBody(json.dumps(body))
        response = self.apiRequestText("post")
        return asset_id

    def updateTaskAssetUpdate(self):
        self.setUrl(self.getUrlConfig())
        asset_id = ''

        asset_id = self.response['inputData']['filename'].replace('mp4', '')

        body = {
            "workflowInstanceId": self.response['workflowInstanceId'],
            "taskId": self.response['taskId'],
            "reasonForIncompletion": "",
            "callbackAfterSeconds": 0,
            "workerId": "remoteCopy",
            "status": "COMPLETED",
            "outputData": {
                "ingest_status": "COMPLETED",
                "assetd": asset_id
            }
        }
        self.setBody(json.dumps(body))
        response = self.apiRequestText("post")
        return asset_id

    def getRemoteCopy(self):
        self.setHeaders({'Content-Type': 'application/json'})
        self.getTask("/poll/remoteCopy")
        return self.response

    def getAssetUpdate(self):
        self.setHeaders({'Content-Type': 'application/json'})
        self.getTask("/poll/integrate_ingest_asset_update")
        return self.response

    def getRemoteDelete(self):
        self.setHeaders({'Content-Type': 'application/json'})
        self.getTask("/poll/remoteDelete")
        return self.response

    def getClearCacheAll(self):
        self.setHeaders({'Content-Type': 'application/json'})
        self.getTask("/poll/ClearCacheAll")
        return self.response

    def getAddDataCatchup(self):
        self.setHeaders({'Content-Type': 'application/json'})
        self.getTask("/poll/AddDataCatchup")
        return self.response
