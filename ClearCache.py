import time
import requests
import json
import apiRequest
import configparser
from mongo import pyMongo

if __name__ == "__main__":
    config = configparser.ConfigParser()
    config.read('config.conf')

    url = "http://" + config['CLEARCACHE']['url']
    drms = ['aes', 'fp', 'wv', 'wv1']

    while True:
        try:
            conductor = apiRequest.conductorRequest()
            response = conductor.getClearCacheAll()

            print(response)
            mongo = pyMongo()
            datas = mongo.find({ '_id': response['inputData']['fileName'].replace('.mp4', '')})

            for data in datas:
                for drm in drms:
                    payload = json.dumps({
                    "asset_id": data['_id'],
                    "vod_type": data['type'],
                    "service_id": data['bp_service_id'],
                    "drm": drm
                    })

                    headers = {
                    'Authorization': 'Bearer ' + config['CLEARCACHE']['token'],
                    'Content-Type': 'application/json'
                    }

                    response = requests.request("POST", url, headers=headers, data=payload)

            asset_id = conductor.updateTaskClearCache()
            print('Clear Cache Complete !!!')
        except Exception as e:
            print(str(e))
        time.sleep(5)