import mongo
import time
from ingest import ingestClass as ingest

from mongo import pyMongo

if __name__ == "__main__":
    try:
        mongo = pyMongo()
        datas = mongo.find({ 'cms_id': { '$exists': True }, 'status_ingest': 'waitting'})

        for data in datas:
            ing = ingest()
            ing.ingestAsset(data['_id'])
            mongo.update_one(
                {'status_ingest': 'complete'},
                {'_id': data['_id']} 
            )
    except Exception as e:
        print(str(e))
    time.sleep(5)

        