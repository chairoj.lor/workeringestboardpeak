import subprocess
import configparser

class cmdUnix():
    def __init__(self):
        self.cmd = []
        self.currentPath = ''
        self.movePath = ''
        self.path = ''
    
    def getConfig(self):
        config = configparser.ConfigParser()
        config.read('config.conf')
        return config

    def setPath(self):
        config = self.getConfig()
        self.currentPath = config['CONDUCTOR']['currentPath']
        self.movePath = config['CONDUCTOR']['movePath']
        self.path = config['CONDUCTOR']['pathBP']

    def setCmd(self, cmd):
        self.cmd = cmd
        self.setPath()
    
    def callCmd(self):
        return subprocess.check_output(self.cmd)

    def copyFile(self, currentPath, movePath):
        self.setCmd([
            'cp',
            '-rp',
            currentPath,
            movePath,
        ])
        self.callCmd()

    def chownFile(self, movePath):
        self.setCmd([
            'chown',
            '-R',
            '1001:1001',
            movePath
        ])
        self.callCmd()

    def deleteFile(self, currentPath):
        self.setCmd([
            'rm', '-rf',
            currentPath,
        ])
        self.callCmd()

    def mkdir(self, Path):
        self.setCmd([
            'mkdir',
            Path,
        ])
        self.callCmd()

    def returnPath(self):
        self.setPath()
        return self.path