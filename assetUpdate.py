import apiRequest
from ingest import ingestClass as ingest
import time

if __name__ == "__main__":
    while True:
        try:
            conductor = apiRequest.conductorRequest()
            response = conductor.getAssetUpdate()
            
            print(response)
            ing = ingest()
            text = ing.ingestAsset(response)

            asset_id = conductor.updateTaskAssetUpdate()

            print(asset_id + " update complete !!!")
        except Exception as e:
            print(str(e))
        time.sleep(5)