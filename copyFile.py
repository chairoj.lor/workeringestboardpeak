import time
import cmdUnix
import readFile

if __name__ == "__main__":
    while True:
        try:            
            readfile = readFile.readFile()
            readfile.setPath()
            readfile.readData()

            cmd = cmdUnix.cmdUnix()
            cmd.setPath()
            cmd.copyFile(readfile.readCsv())
        except:
            pass
        
        time.sleep(5)