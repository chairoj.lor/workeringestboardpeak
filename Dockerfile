FROM python:latest

RUN apt-get update && apt-get install -y locales && rm -rf /var/lib/apt/lists/* \
    && localedef -i th_TH -c -f UTF-8 -A /usr/share/locale/locale.alias th_TH.UTF-8
ENV LANG th_TH.utf8
RUN apt-get update && apt install -y build-essential zlib1g-dev libncurses5-dev libgdbm-dev \
    libnss3-dev libssl-dev libreadline-dev libffi-dev wget python3 python3-pip libaio1 nano vim unzip wget curl
RUN DEBIAN_FRONTEND="noninteractive" apt-get -y install tzdata
ENV TZ Asia/Bangkok
RUN pip3 install pandas pymongo requests pytube==10.8.2 youtube_dl kafka-python pymongo

WORKDIR /opt

RUN mkdir app
RUN mkdir app/currentPath
RUN mkdir app/movePath
RUN mkdir app/qumulo_record_catchup

WORKDIR /opt/app

COPY *.py /opt/app/
COPY config.conf /opt/app/
