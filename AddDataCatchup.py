import time
import apiRequest
from mongo import pyMongo

if __name__ == "__main__":
    while True:
        try:
            conductor = apiRequest.conductorRequest()
            response = conductor.getAddDataCatchup()
            mongo = pyMongo()
            datas = mongo.countCatchup({ '_id' : response['inputData']['filename'].replace('.mp4', '')})
            if datas > 0:
                datas = mongo.findCatchup({ '_id' : response['inputData']['filename'].replace('.mp4', '')})
                for data in datas:
                    mongo.update_one(
                        {
                            'bp_service_id': 'th_gn',
                            'category_id': response['inputData']['ContentType'],
                            'regions': response['inputData']['Region'],
                            'type': response['inputData']['ContentType']},
                        {'_id': data['_id']}
                    )
            else:
                mongo.insertCatchup(
                    { '_id': response['inputData']['filename'].replace('.mp4', ''),
                    'regions': response['inputData']['Region'],
                    'type': response['inputData']['ContentType'],
                    'category_id': response['inputData']['ContentType'],
                    'bp_service_id': 'th_gn'}
                )

            response = conductor.updateTaskAssetUpdate()
        except:
            pass
        time.sleep(5)