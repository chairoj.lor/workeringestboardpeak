import requests
import json
import datetime as dt
import configparser

class ingestClass():
    _config = configparser.ConfigParser()
    _config.read('config.conf')


    def __init__(self):
        self.url = self._config['RMS']['url']
        self.token = self._config['RMS']['token']
        self.headers = {}
        self.response = {}
        self.body = {}

    def setBody(self, body):
        self.body = body

    def setHeaders(self, headers):
        self.headers = headers
    
    def setUrl(self, url):
        self.url = url
    
    def setResponse(self, response):
        self.response = response

    def setToken(self, token):
        self.token = token

    def ingestAsset(self, response):
        env = response['inputData']['env']
        asset_id = response['inputData']['filename'].replace('.mp4', '')
        if not env:
            env = 'production'
        url = self.url + asset_id + '?env=' + env
        headers = {
        'Authorization': "Bearer " + self.token,
        'Content-Type': 'application/json; charset=utf-8'
        }
        response = requests.request("PUT", url, headers=headers, data='')
        return response.text

    