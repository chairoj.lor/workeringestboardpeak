import time
import readFile
import cmdUnix
import apiRequest

if __name__ == "__main__":
    while True:
        try:
            conductor = apiRequest.conductorRequest()
            response = conductor.getRemoteDelete()

            cmd = cmdUnix.cmdUnix()
            cmd.setPath()

            ab_path = response['inputData']['ab_path_file'].split('/')
            if len(ab_path) == 5:
                path = ab_path[1] + "/" + ab_path[2] + '/' + ab_path[3]
            else:
                path = ab_path[1] + '/'  + ab_path[2] + '/'
            cmd.deleteFile("currentPath/" + path)

            asset_id = conductor.updateTask()
            print("currentPath/" + path + " Delete file Complete")
        except Exception as e:
            print(str(e))
        time.sleep(5)