import pymongo
import configparser

class pyMongo():

    def __init__(self):
        self.config = self.setConfig()
        self.url = self.config['MONGO']['url']
        self.client = self.config['MONGO']['client']
        self.collection = self.config['MONGO']['collection']

    def setConfig(self):
        config = configparser.ConfigParser()
        config.read('config.conf')
        return config

    def createConnection(self):
        client = pymongo.MongoClient(self.url)
        return client
    
    def selectDBCollection(self, client):
        db = client[self.client]
        col = db[self.collection]
        return col

    def selectDBCollectionAdd(self, client, collection):
        db = client[self.client]
        col = db[collection]
        return col

    def findall(self):
        connection = self.createConnection()
        col = self.selectDBCollection(connection)
        datas = col.find(no_cursor_timeout=True)
        connection.close()
        return datas
    
    def find(self, condition):
        connection = self.createConnection()
        col = self.selectDBCollection(connection)
        datas = col.find(condition, no_cursor_timeout=True)
        connection.close()
        return datas

    def update_one(self, newdata, olddata):
        connection = self.createConnection()
        col = self.selectDBCollection(connection)
        newdata = {"$set": newdata}
        col.update_one(olddata, newdata)
        connection.close()

    def insertCatchup(self, data):
        connection = self.createConnection()
        col = self.selectDBCollectionAdd(connection, 'asset')
        col.insert_one(data)
        connection.close()

    def findCatchup(self, condition):
        connection = self.createConnection()
        col = self.selectDBCollectionAdd(connection, 'asset')
        datas = col.find(condition, no_cursor_timeout=True)
        connection.close()
        return datas

    def countCatchup(self, condition):
        connection = self.createConnection()
        col = self.selectDBCollectionAdd(connection, 'asset')
        datas = col.find(condition, no_cursor_timeout=True).count()
        connection.close()
        return datas
# ASS201902000054

# for i in pyMongo().find(
#     { 'cms_id': { '$exists': False }, 'status_ingest': 'complete' }
# ):
#     print(i['_id'])
#     pyMongo().update_one({'status_ingest': 'waitting'},{'_id': i['_id']} )
#     break