import time
import apiRequest
import cmdUnix

if __name__ == "__main__":
    path = {
        "region/IndonesiaBP/": ["region/Indonesia/"],
        "region/PhilippinesBP/": ["region/Philippines/"],
        "region/VietnamBP/": ["region/Vietnam/"],
        "region/MyanmarBP/": ["region/Myanmar/"],
        "region/CambodiaBP/": ["region/Cambodia/"],
        "region/clip_khmBP/": ["region/clip_khm/"],
        "sourceBP/": ["source"],
        "clipBP/": ["clip/"],
        "skyclipBP/": ["skyclip/"],
        "fullmatchBP/": ["fullmatch/"],
        "vr4kBP/": ["vr4k/"],
        "lastidolBP/": ["lastidol/"],
        "tnndownloadBP/": ["tnndownload/"],
        "logo-trueid-plusBP/": ["logo-trueid-plus/"],
        "logo-trueidBP/": ["logo-trueid/"],
        "testrecordBP/": "testrecord/"
    }
    while True:
        try:
            conductor = apiRequest.conductorRequest()
            response = conductor.getRemoteCopy()

            print(response)
            cmd = cmdUnix.cmdUnix()
            cmd.setPath()
            if 'testrecordBP' in response['inputData']['ab_path_file']:
                ab_path = response['inputData']['ab_path_file'].split('/')
                movepath = "./qumulo_record_catchup/"  + ab_path[1] + "/"
                try:
                    cmd.mkdir("./movePath/" + response['inputData']['ab_path_file'].split('/')[2].replace('.mp4', ''))
                except:
                    print('folder already exsit')
                try:
                    # cmd.copyFile("qumulo_record_catchup" + response['inputData']['ab_path_file'][:len(response['inputData']['ab_path_file'])-1], "./movePath/" + response['inputData']['ab_path_file'].split('/')[2].replace('.mp4', '') + "/" + response['inputData']['ab_path_file'].split('/')[2])
                    cmd.copyFile("qumulo_record_catchup" + response['inputData']['ab_path_file'][:len(response['inputData']['ab_path_file'])-1], "./movePath/")
                    cmd.chownFile("./movePath/"  + ab_path[2].replace('.mp4', ''))
                except:
                    print('cannot copy file to vod_store')
                try:
                    cmd.copyFile("qumulo_record_catchup" + response['inputData']['ab_path_file'][:len(response['inputData']['ab_path_file'])-1], "qumulo_record_catchup/" + "testrecord/")
                except Exception as e:
                    print(str(e))
            elif 'region' in response['inputData']['ab_path_file']:
                ab_path = response['inputData']['ab_path_file'].split('/')
                movepath = "./currentPath/"  + ab_path[1] + "/" + ab_path[2][:-2] + "/"
                try:
                    cmd.copyFile("currentPath" + response['inputData']['ab_path_file'][:len(response['inputData']['ab_path_file'])-1]+ "/" + response['inputData']['asset_id'], movepath )
                    cmd.chownFile(movepath)
                except:
                    print('cannot copy file to {}'.format(movepath))
            else:
                ab_path = response['inputData']['ab_path_file'].split('/')
                movepath = "./movePath/"  + ab_path[2] + "/" + ab_path[3]
                try:
                    cmd.mkdir("./movePath/" + response['inputData']['ab_path_file'].split('/')[2])
                except:
                    print('folder already exsit')
                try:
                    cmd.copyFile("currentPath" + response['inputData']['ab_path_file'][:len(response['inputData']['ab_path_file'])-1]+ "/" + response['inputData']['asset_id'], "movePath/" + response['inputData']['ab_path_file'].split('/')[2])
                    cmd.chownFile("./movePath/"  + ab_path[2])
                except:
                    print('cannot copy file to vod_store')

                try:
                    cmd.copyFile("currentPath" + response['inputData']['ab_path_file'][:len(response['inputData']['ab_path_file'])-1] + "/" + response['inputData']['asset_id'], "currentPath/" + path[response['inputData']['ab_path_file'].split('/')[1] + "/"][0])
                except Exception as e:
                    print(str(e))
            response = conductor.updateTask()
            print("Copy Complete !!!")
        except Exception as e:
            print(str(e))

        time.sleep(5)
