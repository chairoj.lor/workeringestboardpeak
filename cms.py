import requests
import json
import datetime as dt
import configparser
class CMSClass():

    def __init__(self):
        self.url = ''
        self.token = ''

    def getConfig(self):
        config = configparser.ConfigParser()
        config.read('config.conf')
        return config

    def setUrl(self):
        config = self.getConfig()
        self.url = config['CMS']['url']

    def setToken(self):
        config = self.getConfig()
        self.token = config['CMS']['token']

    def setAll(self):
        self.setUrl()
        self.setToken()

    def update_status_cms(self, body, cms_id):
        url = self.url.format(cms_id)
        headers = {
        'Authorization': 'Bearer ' + self.token,
        'Content-Type': 'application/json; charset=utf-8'
        }

        response = requests.request("PUT", url, headers=headers, data=body)
        return response.text

    def get_status_data(self, cms_id):
        url = self.url.format(cms_id)
        headers = {
        'Authorization': 'Bearer ' + self.token,
        'Content-Type': 'application/json; charset=utf-8'
        }

        response = requests.request("GET", url, headers=headers, data={})
        return response.text

    def update(self, cms_id):
        value = self.get_status_data(cms_id)
        value = json.loads(value)

        body_update = {
        'content_type': value['data']['content_type'],
        'country': 'th',
        'status': value['data']['status'],
        'publish_date': str((dt.datetime.now()-dt.timedelta(hours=7)).strftime('%Y-%m-%dT%H:%M:%SZ')),
        'update_date': str((dt.datetime.now()-dt.timedelta(hours=7)).strftime('%Y-%m-%dT%H:%M:%SZ')),
        'update_by': 'MAM'
        }
        if 'title' in value['data']:
            body_update.update({'title': value['data']['title']})
        else:
            body_update.update({'lang_data': {
                'en': {
                    'title': value['data']['lang_data']['en']['title']
                },
                'th': {
                    'title': value['data']['lang_data']['th']['title'],
                }
            }})

        body_update = json.dumps(body_update, indent=4, sort_keys=True)
        response = self.update_status_cms(body_update, cms_id)
        return response
